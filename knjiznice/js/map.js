
/* global L, distance */
var zemljevid;
var options;

const UKC_X = 46.0535957;
const UKC_Y = 14.5198024;

function pridobiPodatke(callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        
        // nastavimo ustrezna polja (število najdenih zadetkov)
        
        // vrnemo rezultat
        callback(json);
    }
  };
  xobj.send(null);
}

window.addEventListener('load', function(){
    var options = {
        center: [UKC_X, UKC_Y],
        zoom: 12
    }
    var layer = new L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    zemljevid = new L.map("mapid", options).setView([UKC_X, UKC_Y], 13);
    zemljevid.addLayer(layer);
    pridobiPodatke(function(json){
       
        for (var i = 0; i < json.features.length; i++){
            for (var j = 0; j < json.features[i].geometry.coordinates.length; j++){
                for (var k = 0; k < json.features[i].geometry.coordinates[j].length; k++){
                    for (var l = 0; l < json.features[i].geometry.coordinates[j][k].length; l++)
                        var prvi = json.features[i].geometry.coordinates[0][0][0];
                        var drugi = json.features[i].geometry.coordinates[0][0][1];
                        console.log(prvi)
                        console.log(drugi)
                        var polygon = L.polygon([prvi, drugi]).addTo(zemljevid);
                }
            }
        }
    });
});
